package net.ericsonj.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author estebanvega
 */
@WebServlet("/code")
public class ServletCode extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = new User();
        user.setName("ERICSON");
        req.setAttribute("user", user);
        req.getRequestDispatcher("test.jsp").forward(req, resp);
        resp.getWriter().print("");
    }

}
